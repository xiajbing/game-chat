import Common.XmlRead;

import java.io.File;
import java.util.Map;

public class ReadXml {
    public static void main(String[] args){
        File f = new File("src/main/resources/server.xml");
        Map<String , Map<String,Object>> CONSTANT = XmlRead.getXml(f);
        System.out.println(CONSTANT);
    }
}
