package GameDb;

import GameDb.Task.*;
import GameDb.Task.common.DbTask;
import Protobuf.DbProto;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;


public class DbHandler extends SimpleChannelInboundHandler<DbProto.Msg> {

    public static DbQueue dbQueue = new DbQueue(4);
    @Override
    public void channelActive(ChannelHandlerContext ctx){
        ServerInit serverInit =new ServerInit(ctx);
        /*
         * 服务端打开时读出所有已存在玩家id*/
        dbQueue.loadTask(serverInit);

    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, DbProto.Msg msg){
        DbProto.Msg.MsgType type = msg.getMsgType();
        switch (type){
            case res:{
                DbTask dbTask = new DbRegister(ctx,msg);
                System.out.println("register");
                dbQueue.loadTask(dbTask);
                break;
            }
            case log:{
                DbTask login = new DbLogin(ctx,msg);
                System.out.println("login");
                dbQueue.loadTask(login);
                break;
            }
            case priChat: {
                DbTask privateChatDb = new PrivateChatDb(msg);
                System.out.println("chat");
                dbQueue.loadTask(privateChatDb);
                break;
            }
            default:break;
        }
    }

}
