package GameDb.Task;

import GameDb.JDBC.JDBCTemplate;
import GameDb.Task.common.DbTask;
import Protobuf.DbProto;
import io.netty.util.concurrent.DefaultEventExecutorGroup;
import io.netty.util.concurrent.EventExecutorGroup;
import io.netty.util.concurrent.Future;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Callable;

/**
 * @author jedi
 */
public class PrivateChatDb implements DbTask {
    private final DbProto.Msg msg;

    public PrivateChatDb(DbProto.Msg msg){
        this.msg = msg;
    }


    @Override
    public void run(Connection conn) throws Exception {
        DbProto.ChatMsg priMsg = msg.getChatMsg();
        //设置日期格式
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        String sql = "INSERT INTO private_chat(sender_id,receiver_id,message,time,suc) VALUE(?,?,?,?,1)";
        Object[] objects = {priMsg.getSenderId(),priMsg.getReceiverId(),priMsg.getMsg(),df.format(date)};
        JDBCTemplate jdbcTemplate = new JDBCTemplate();
        jdbcTemplate.insert(conn,sql,objects);
    }
}
