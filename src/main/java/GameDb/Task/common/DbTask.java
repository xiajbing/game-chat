package GameDb.Task.common;

import java.sql.Connection;

/**
 * @author jedi
 */
public interface DbTask {
    public int ERRORCOUNT = 0;

    public void run(Connection conn) throws Exception;
}
