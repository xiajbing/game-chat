package GameDb.Task;

import GameDb.Task.common.DbTask;

import javax.xml.crypto.Data;
import java.sql.Connection;
import java.sql.Date;

public class Test implements DbTask {
    private final int a;

    public Test(int a){
        this.a = a;
    }
    @Override
    public void run(Connection conn) throws Exception {
        Thread.sleep(10);
        System.out.println(a);
        conn.close();
    }
}
