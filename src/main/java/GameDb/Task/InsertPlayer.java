package GameDb.Task;

import GameDb.JDBC.JDBCTemplate;
import GameDb.Task.common.DbTask;
import Protobuf.DbProto;
import io.netty.channel.ChannelHandlerContext;
import java.sql.Connection;


public class InsertPlayer implements DbTask {
    private final ChannelHandlerContext ctx;
    private final DbProto.Msg msg;
    private final Object[] objects;

    public InsertPlayer(ChannelHandlerContext ctx,DbProto.Msg msg,Object...objects){
        this.ctx = ctx;
        this.msg = msg;
        this.objects = objects;
    }

    @Override
    public void run(Connection conn) throws Exception {
        String sql = "INSERT IGNORE INTO player VALUES(?,?,?,?)";
        String clientChannelId = msg.getChannelId();
        JDBCTemplate jdbcTemplate = new JDBCTemplate();
        jdbcTemplate.selectDbOperate(conn,sql,1,objects);
        DbProto.Msg resSucMsg =  DbProto.Msg.newBuilder().setMsgType(DbProto.Msg.MsgType.resSuc).setChannelId(clientChannelId)
                .setPlayer(DbProto.Player.newBuilder().setId((long)objects[0]).setName(msg.getPlayer().getName())
                        .setUsername(msg.getPlayer().getUsername()).setPsd(msg.getPlayer().getPsd())
                        .setTid(10000L).setGuid(10000L).build()).build();
        ctx.writeAndFlush(resSucMsg);
    }
}
