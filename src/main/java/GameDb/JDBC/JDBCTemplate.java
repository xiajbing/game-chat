package GameDb.JDBC;


import java.sql.*;
import java.util.*;

/**
 * @author jedi
 */
public class JDBCTemplate {

    private PreparedStatement prep = null;
    private ResultSet rs = null;

    private long update(Connection conn,String sql,Object...params) throws Exception {
        JDBCUtils jdbcUtils = new JDBCUtils();
        //conn = jdbcUtils.getDruidConn();
        prep = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        for(int i = 0;i<params.length;i++){
            prep.setObject(i+1,params[i]);
        }
        prep.executeUpdate();
        rs = prep.getGeneratedKeys();
        long id = 0;
        if(rs.next()){
            id = rs.getLong(1);
        }
        jdbcUtils.release(rs,prep,conn);
        return id;
    }

    public Map<String,Object> getOne(Connection conn,String sql, Object...params) throws Exception {
        JDBCUtils jdbcUtils = new JDBCUtils();
        //conn = jdbcUtils.getDruidConn();
        prep = conn.prepareStatement(sql);
        for(int i = 0; i<params.length;i++){
            prep.setObject(i+1,params[i]);
        }
        rs = prep.executeQuery();
        Map<String,Object> map = new HashMap<>(16);
        while (rs.next()){
            map = getQuery(rs);
        }
        jdbcUtils.release(rs,prep,conn);
        return map;
    }

    public Map<String,Object> getQuery(ResultSet rs) throws Exception {
        ResultSetMetaData resultSetMetaData = rs.getMetaData();
        Map<String,Object> map = new HashMap<>(16);
        for(int i =0;i<resultSetMetaData.getColumnCount();i++){
            String colName = resultSetMetaData.getColumnName(i+1);
            Object colVal = rs.getObject(colName);
            if(colVal == null) {
                colVal = "";
            }
            map.put(colName,colVal);
        }
        return map;

    }

    public List<Map<String, Object>> getList(Connection conn,String sql, Object...params) throws Exception {
        JDBCUtils jdbcUtils = new JDBCUtils();
        //conn = jdbcUtils.getDruidConn();//连接池
        //conn = jdbcUtils.getConnection();//直接建立数据库连接
        prep = conn.prepareStatement(sql);
        List<Map<String,Object>> list = new ArrayList<>();
        for(int i=0;i<params.length;i++){
            prep.setObject(i+1,params[i]);
        }
        rs = prep.executeQuery();
        while (rs.next()){
            Map<String,Object> map = getQuery(rs);
            list.add(map);
        }
        jdbcUtils.release(rs,prep,conn);
        return list;
    }

    public long insert(Connection conn,String sql,Object...params) throws Exception {
        return update(conn,sql,params);
    }

    public void delete(Connection conn,String sql,Object...params) throws Exception {
        update(conn,sql,params);
    }


    public Object selectDbOperate(Connection conn,String sql, int type, Object...params) throws Exception {
        Object map = null;
        switch (type){
            case 1:map = insert(conn,sql,params);break;
            case 2:delete(conn,sql,params);break;
            case 3:map = getOne(conn,sql,params);break;
            case 4:map = getList(conn,sql,params);break;
            case 5:update(conn,sql,params);break;
            default:
                throw new IllegalStateException("Unexpected value: " + type);
        }
        return map;
    }
}
