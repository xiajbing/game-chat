package Server;

import Common.Server;
import Protobuf.ChatProto;
import Protobuf.DbProto;
import io.netty.bootstrap.Bootstrap;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.protobuf.ProtobufDecoder;
import io.netty.handler.codec.protobuf.ProtobufEncoder;
import io.netty.handler.codec.protobuf.ProtobufVarint32FrameDecoder;
import io.netty.handler.codec.protobuf.ProtobufVarint32LengthFieldPrepender;

import java.net.InetSocketAddress;

public class GameServer {
//    private static final int CHAT_PORT = Server.HostAndPort.CHATPORT.getPort();
//    private static final String HOST = Server.HostAndPort.CHATHOST.getHost();
//    private static final int DB_PORT = Server.HostAndPort.DBPORT.getPort();

    private static final int CHAT_PORT = Integer.parseInt((String)Server.HostAndPort.getValueByName("chatPort"));
    private static final String HOST = (String) Server.HostAndPort.getValueByName("chatHost");
    private static final int DB_PORT = Integer.parseInt((String)Server.HostAndPort.getValueByName("dbPort"));

    public static void main(String[] args){
        NioEventLoopGroup bossGroup =new NioEventLoopGroup(1);
        NioEventLoopGroup workerGroup =new NioEventLoopGroup();

        try {
            ServerBootstrap serverBootstrap = new ServerBootstrap();
            serverBootstrap
                    .group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .option(ChannelOption.SO_RCVBUF,1024*32) //接收缓冲区
                    .childOption(ChannelOption.SO_SNDBUF,1024*32) //发送缓冲区
                    .option(ChannelOption.SO_BACKLOG, 1024) //最大等待连接
                    .childOption(ChannelOption.SO_KEEPALIVE, true) //心跳
                    .childOption(ChannelOption.TCP_NODELAY, true) //禁用nagle算法
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        public void initChannel(SocketChannel ch){

                            ch.pipeline().addLast(new ProtobufVarint32FrameDecoder());
                            ch.pipeline().addLast(new ProtobufDecoder(ChatProto.Msg.getDefaultInstance()));
                            ch.pipeline().addLast(new ProtobufVarint32LengthFieldPrepender());
                            ch.pipeline().addLast(new ProtobufEncoder());

                            ch.pipeline().addLast(new ServerHandler());

                        }
                    });

            new Thread(GameServer::connDb).start();

            //int port = Server.getChatPort();

            ChannelFuture future = serverBootstrap.bind(CHAT_PORT).sync();
            future.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            bossGroup.shutdownGracefully();//关闭
            workerGroup.shutdownGracefully();
        }

    }


    public static void connDb(){

        NioEventLoopGroup dbGroup = new NioEventLoopGroup();
        try {
            Bootstrap bootstrap = new Bootstrap();
            bootstrap.group(dbGroup)
                    .channel(NioSocketChannel.class)
                    .option(ChannelOption.SO_KEEPALIVE, true)
                    .option(ChannelOption.TCP_NODELAY, true)
                    .option(ChannelOption.SO_RCVBUF, 32 * 1024)
                    .handler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        public void initChannel(SocketChannel ch) {
                            ch.pipeline().addLast(new ProtobufVarint32FrameDecoder());
                            ch.pipeline().addLast(new ProtobufDecoder(DbProto.Msg.getDefaultInstance()));
                            ch.pipeline().addLast(new ProtobufVarint32LengthFieldPrepender());
                            ch.pipeline().addLast(new ProtobufEncoder());

                            ch.pipeline().addLast(new DbClientHandler());
                        }
                    });
            ChannelFuture futureDb = bootstrap.connect(new InetSocketAddress(HOST, DB_PORT)).sync();
            futureDb.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            dbGroup.shutdownGracefully();
        }
    }


}
