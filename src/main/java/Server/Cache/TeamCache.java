package Server.Cache;

import Server.Common.Player;
import Server.Common.Team;

import java.util.*;
import java.util.concurrent.*;

public class TeamCache {

    private TeamCache(){}

    private static class InnerUser{
        private static final TeamCache SINGLETON = new TeamCache();
    }

    public static TeamCache getInstance(){
        return TeamCache.InnerUser.SINGLETON;
    }

    private final Map<Long, Team> teamMap = new HashMap<>();


    public void saveTeam(Player player){
        Team team;
        long tid = player.getTid();
        if(!teamMap.containsKey(tid)){
            List<Player> list = new ArrayList<>();
            team =new Team.Builder().setTid(tid).setName("team"+ teamMap.size()).setList(list).build();
        }else {
            team = teamMap.get(tid);
        }
        team.getList().add(player);
        putTeam(tid,team);
    }

    public Team creatTim(Long tid){
        List<Player> tList = new ArrayList<>();
        Team team =new Team.Builder().setTid(tid).setName("team"+ teamMap.size()).setList(tList).build();
        putTeam(tid,team);
        return team;
    }

    public void putTeam(long tid,Team team){

        teamMap.put(tid,team);

    }

    public Team getTeam(long tid){
        Team team = teamMap.get(tid);
        if(team == null){
            team = creatTim(tid);
        }
        return team;

    }
}