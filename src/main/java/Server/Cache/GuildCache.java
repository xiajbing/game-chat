package Server.Cache;

import Server.Common.Guild;
import Server.Common.Player;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

public class GuildCache {
    private GuildCache(){}

    private static class InnerUser{
        private static final GuildCache SINGLETON = new GuildCache();
    }

    public static GuildCache getInstance(){
        return GuildCache.InnerUser.SINGLETON;
    }

    private final Map<Long, Guild> guildMap = new HashMap<>();


    public void saveGuild(Player player){
        Guild guild;
        long guid = player.getGuid();
        if(!guildMap.containsKey(guid)){
            List<Player> list = new ArrayList<>();
            guild =new Guild.Builder().setGuid(guid).setName("union"+ guildMap.size()).setList(list).build();
        }else {
            guild = guildMap.get(guid);
        }
        guild.getList().add(player);
        putGuild(guid, guild);
    }

    public Guild creatUnion(Long guid){
        List<Player> gList = new ArrayList<>();
        Guild guild =new Guild.Builder().setGuid(guid).setName("team"+ guildMap.size()).setList(gList).build();
        putGuild(guid, guild);
        return guild;
    }

    public void putGuild(long guid,Guild guild){
        guildMap.put(guid,guild);
    }


    public Guild getGuild(long guid){
        Guild guild = guildMap.get(guid);
        if(guild == null){
            guild = creatUnion(guid);
        }
        return guild;
    }
}