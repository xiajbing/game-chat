package Server.Cache;
import io.netty.channel.Channel;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ChannelCache {

    private ChannelCache(){}

    private static class InnerUser{
        private static final ChannelCache SINGLETON = new ChannelCache();
    }

    public static ChannelCache getInstance(){
        return ChannelCache.InnerUser.SINGLETON;
    }

    private final Map<String, Channel> channelMap= new HashMap<>();

    public synchronized void putChannel(String channelId,Channel channel){
        channelMap.put(channelId,channel);
    }

    public Channel getChannel(String channelId){
        return  channelMap.get(channelId);
    }

    public void removeChannel(String channelId){
        channelMap.remove(channelId);
    }


}
