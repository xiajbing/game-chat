package Server.Cache;

public enum ChannelState {
    /**上线*/
    ONLINE,
    /**下线*/
    OFFLINE,
    /**运行*/
    RUNNING

}
