package Server.Cache;

import Server.Common.Player;
import io.netty.channel.Channel;
import java.util.*;


public class PlayerCache {

    private PlayerCache(){}

    private static class InnerUser{
        private static final PlayerCache SINGLETON = new PlayerCache();
    }

    public static PlayerCache getInstance(){
        return PlayerCache.InnerUser.SINGLETON;
    }

    private final Map<Long, Player> playerCache = new HashMap<>();
    private final Map<Channel,Player> channelPlayer = new HashMap<>();

    public void putPlayer(Player player){
        playerCache.put(player.getId(),player);
    }

    public void putCtxPlayer(Channel ctx, Player player){
        channelPlayer.put(ctx,player);
    }

    public Player getPlayer(Object object){
        if(object instanceof Channel) {
            return channelPlayer.get(object);
        }
        if(object instanceof Long){
            return playerCache.get(object);
        }
        return null;
    }

    public List<Player> getAllPlayer(){
        if(playerCache.size() == 0) {
            return null;
        }
        Collection<Player> values= playerCache.values();
        return new ArrayList<>(values);
    }

    public boolean isContainsPlayer(long id){
        return playerCache.containsKey(id);
    }
}