package Server.Cache;

import io.netty.channel.Channel;
import io.netty.util.Attribute;
import io.netty.util.AttributeKey;


public class ChannelUtil {

    private static final AttributeKey<MsgSession> CHANNEL_SESSION = AttributeKey.valueOf("player");


    public static boolean online(Channel channel,MsgSession session) {
        Attribute<MsgSession> val = channel.attr(CHANNEL_SESSION);
        return val.compareAndSet(null,session);
    }

    public static MsgSession getSession(Channel channel){
        Attribute<MsgSession> val = channel.attr(CHANNEL_SESSION);
        return val.get();
    }

}
