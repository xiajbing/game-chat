package Server.Chat;

import Protobuf.ChatProto;
import Server.Cache.MsgSession;
import Server.Common.World;
import Server.ServerHandler;

public class WorldChat implements Chat {
    @Override
    public void sendPacket(MsgSession session) {
        ChatProto.Msg msg = session.getMsg();
        ChatProto.Chat worChat = msg.getChat();
        World.getInstance().broadcast(worChat.getReceiver(),msg.getMsgType(),worChat);
        System.out.println(worChat.getSenderId()+"[世界]:" +worChat.getMsg());

    }
}
