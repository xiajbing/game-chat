package Server.Chat;

import Protobuf.ChatProto;
import Server.Cache.ChannelUtil;
import Server.Cache.MsgSession;
import Server.ServerHandler;

public class MapChat implements Chat {


    @Override
    public void sendPacket(MsgSession session) {
        ChatProto.Msg msg = session.getMsg();
        ChatProto.Chat chat = msg.getChat();
        ServerHandler.scene.broadcast(chat.getSenderId(),msg.getMsgType(),chat);
        System.out.println(chat.getSenderId()+"[附近]:" +chat.getMsg());

    }
}
