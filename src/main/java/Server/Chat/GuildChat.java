package Server.Chat;

import Protobuf.ChatProto;
import Server.Cache.MsgSession;
import Server.Cache.PlayerCache;
import Server.Cache.GuildCache;
import Server.Common.Guild;
import Server.Common.Player;

public class GuildChat implements Chat {
    @Override
    public void sendPacket(MsgSession session) {
        ChatProto.Msg msg = session.getMsg();
        long id = msg.getChat().getSenderId();
        Player player= PlayerCache.getInstance().getPlayer(id);
        Guild union = GuildCache.getInstance().getGuild(player.getGuid());
        union.broadcast(id,msg.getMsgType(),msg.getChat());
    }
}
