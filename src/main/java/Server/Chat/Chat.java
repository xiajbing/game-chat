package Server.Chat;

import Protobuf.ChatProto;
import Server.Cache.MsgSession;


public interface Chat {
    /**用于客户端与服务端交互，发送消息或者信号*/
    void sendPacket(MsgSession session);

}
