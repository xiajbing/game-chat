package Server.Chat;

import Protobuf.ChatProto;
import Server.Cache.MsgSession;
import Server.Cache.PlayerCache;
import Server.Cache.TeamCache;
import Server.Common.Player;
import Server.Common.Team;


public class TeamChat implements Chat {
    @Override
    public void sendPacket(MsgSession session) {
        ChatProto.Msg msg = session.getMsg();
        long id = msg.getChat().getSenderId();
        Player player= PlayerCache.getInstance().getPlayer(id);
        System.out.println(id);
        Team team = TeamCache.getInstance().getTeam(player.getTid());
        team.broadcast(id,msg.getMsgType(),msg.getChat());
    }
}
