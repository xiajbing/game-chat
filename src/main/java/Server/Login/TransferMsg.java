package Server.Login;

import Protobuf.ChatProto;
import Server.Cache.ChannelCache;
import Server.Cache.MsgSession;
import Server.DbClientHandler;


public abstract class TransferMsg {

    /**登录或注册完成后，将登陆结果返回*/
    public void loginResponse(ChatProto.RespLogin.LoginResult result,boolean isSuc,String channelId,Object...params) {
        ChatProto.Msg.MsgType msgType = ChatProto.Msg.MsgType.logon;
        ChatProto.PlayerState playerState = ChatProto.PlayerState.newBuilder().build();
        //注册失败，result = usernameIsExist(0)
        if(result.getNumber() == 0){
            msgType = ChatProto.Msg.MsgType.res;
        }
        //登陆或注册成功
        if(isSuc){
            msgType = ChatProto.Msg.MsgType.suc;
            playerState = ChatProto.PlayerState.newBuilder().setId((long)params[0]).setName((String)params[1]).build();
        }
        ChatProto.Msg regSucMsg = ChatProto.Msg.newBuilder().setMsgType(msgType)
                .setRespLogin(ChatProto.RespLogin.newBuilder().setLoginResult(result).setSuc(isSuc)
                        .setPlayer(playerState).build()).build();
        ChannelCache.getInstance().getChannel(channelId).writeAndFlush(regSucMsg);
    }

    /**将登陆或注册消息发送到数据库服务器*/
    public abstract void sendDbPacket(MsgSession session);
}
