package Server.Common;

import java.util.List;

public class Team extends Organization{
    private final Long tid;
    private final String name;

    public static final class Builder{
        private Long tid;
        private String name;
        private  List<Player> list;

        public Builder setTid(Long val){
            tid = val;
            return this;
        }
        public Builder setName(String val){
            name = val;
            return this;
        }
        public Builder setList(List<Player> val){
            list = val;
            return this;
        }

        public Team build(){
            return new Team(this);
        }
    }
    public Team(Builder builder){
        tid = builder.tid;
        name = builder.name;
        list = builder.list;
    }


    public Long getTid(){
        return tid;
    }
    public String getName(){
        return name;
    }


}
