package Server.Common;

public class AOINode{
    public int x;
    public int y;
    public long key;

    public AOINode xPrev;
    public AOINode xNext;
    public AOINode yPrev;
    public AOINode yNext;

    public AOINode(long key, int x, int y){
        this.key=key;
        this.x=x;
        this.y=y;
        xPrev=xNext=null;
        yPrev=yNext=null;
    }
}



