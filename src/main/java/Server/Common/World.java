package Server.Common;


import Protobuf.ChatProto;

import java.util.ArrayList;
import java.util.List;


public class World implements Broadcast{

    private World(){}
    private static class InnerUser{
        private static final World SINGLETON = new World();
    }
    public static World getInstance(){
        return World.InnerUser.SINGLETON;
    }


    public List<Long> allId = new ArrayList<>();
    public List<String> usernames = new ArrayList<>();

    private final PlayerList playerList = new PlayerList();

    public PlayerList getOnlinePlayer(){
        return playerList;
    }

    public void addLoginOnPlayer(Player player){
        playerList.addMember(player);
    }

    public void removeLoginOutPlayer(Player player){
        playerList.removeMember(player);
    }

    @Override
    public void broadcast(Long id, ChatProto.Msg.MsgType msgType, ChatProto.Chat msg){
        playerList.broadcast(id,msgType,msg);
    }
}
