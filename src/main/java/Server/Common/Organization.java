package Server.Common;

import Protobuf.ChatProto;

import java.util.List;

public abstract class Organization implements Broadcast{
    protected List<Player> list;

    public void addMember(Player player){
        this.list.add(player);
    }


    public void removeMember(Player player){
        if(list.contains(player)) {
            this.list.remove(player);
        }
    }

    public List<Player> getList(){
        return list;
    }

    public List<Player> getList(Long id){
        return list;
    }

    @Override
    public void broadcast(Long id, ChatProto.Msg.MsgType msgType, ChatProto.Chat msg){
        list =  getList(id);
        ChatProto.Msg chatMsg = ChatProto.Msg.newBuilder().setMsgType(msgType)
                .setRespChat(ChatProto.RespChat.newBuilder().setSender(msg.getSenderName()).setMsg(msg.getMsg()).build())
                .build();
        for(Player r:list){
            r.sendMsg(chatMsg);
        }
    }



}
