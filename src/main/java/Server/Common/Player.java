package Server.Common;

import Protobuf.ChatProto;
import Protobuf.DbProto;
import Server.Cache.*;
import Server.ServerHandler;
import io.netty.channel.Channel;

public class Player {

    private Long id;
    private String name;
    private Long tid;
    private Long guid;
    private String username;
    private String psd;
    private int x;
    private int y;
    private Channel channel;

    public Player(){}

    public static  final class Builder{
        private Long id;
        private String name;
        private Long tid;
        private Long guid;
        private String username;
        private String psd;
        private int x;
        private int y;
        private Channel ctx;

        public Builder(){}

        public Builder setId(Long val){
            id = val;
            return this;
        }
        public Builder setName(String val){
            name = val;
            return this;
        }
        public Builder setGuid(Long val){
            guid = val;
            return this;
        }
        public Builder setTid(Long val){
            tid = val;
            return this;
        }
        public Builder setUsername(String val){
            username = val;
            return this;
        }
        public Builder setPsd(String val){
            psd = val;
            return this;
        }
        public Builder setX(int val){
            x = val;
            return this;
        }
        public Builder setY(int val){
            y = val;
            return this;
        }
        public Builder setCtx(Channel val){
            ctx = val;
            return this;
        }

        public Player build(){
            return new Player(this);
        }

    }


    private Player(Builder builder){
        id = builder.id;
        name = builder.name;
        guid = builder.guid;
        tid = builder.tid;
        username = builder.username;
        psd = builder.psd;
        x = builder.x;
        y = builder.y;
        channel = builder.ctx;
    }

    public void setId(Long id){
        this.id = id;
    }
    public void setName(String name){
        this.name = name;
    }
    public void setTid(Long tid) {
        this.tid = tid;
    }
    public void setGuid(Long guid) {
        this.guid = guid;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public void setPsd(String psd) {
        this.psd = psd;
    }
    public void setPosition(int x, int y){
        this.x = x;
        this.y = y;
    }
    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public Long getId(){
        return id;
    }
    public String getName(){
        return name;
    }
    public Long getTid(){
        return tid;
    }
    public Long getGuid(){
        return guid;
    }
    public String getUsername(){
        return username;
    }
    public String getPsd(){
        return psd;
    }
    public int getX(){return x;}
    public int getY(){return y;}
    public Channel getChannel(){return channel;}


    public void sendMsg(ChatProto.Msg msg){
        channel.writeAndFlush(msg);
    }


    public static Player create(DbProto.Msg msg, Long id, String username) {
        int x = (int) (Math.random()*10);
        int y = (int) (Math.random()*10);
        String psd = msg.getPlayer().getPsd();
        String name = msg.getPlayer().getName();
        Long tid = msg.getPlayer().getTid();
        Long guid = msg.getPlayer().getGuid();
        System.out.println(tid+" "+guid);
        Channel channel = ChannelCache.getInstance().getChannel(msg.getChannelId());

        return new Builder().setId(id).setX(x).setY(y).setUsername(username)
                .setPsd(psd).setName(name).setGuid(guid).setTid(tid)
                .setCtx(channel)
                .build();
    }


    public void load(){//把角色数据保存到缓存中
        TeamCache.getInstance().saveTeam(this);
        GuildCache.getInstance().saveGuild(this);
        World.getInstance().addLoginOnPlayer(this);
        PlayerCache.getInstance().putPlayer(this);
        PlayerCache.getInstance().putCtxPlayer(this.channel,this);
        ServerHandler.scene.addMember(this);

    }
}
