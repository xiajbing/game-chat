package Server.Common;

import java.util.List;

public class Guild extends Organization {
    private final Long guid;
    private final String name;
    //private  List<Long> list;

    public Long getGuid(){
        return guid;
    }
    public String getName(){
        return name;
    }

    public static final class Builder{
        private Long guid;
        private String name;
        private  List<Player> list;

        public Builder setGuid(Long val){
            guid = val;
            return this;
        }
        public Builder setName(String val){
            name = val;
            return this;
        }
        public Builder setList(List<Player> val){
            list = val;
            return this;
        }

        public Guild build(){
            return new Guild(this);
        }
    }

    public Guild(Builder builder){
        guid = builder.guid;
        name = builder.name;
        list = builder.list;
    }
}
