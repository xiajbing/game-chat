package Server.Common;

import Protobuf.ChatProto;

public interface Broadcast {
    public void broadcast(Long id, ChatProto.Msg.MsgType msgType, ChatProto.Chat msg);
}
