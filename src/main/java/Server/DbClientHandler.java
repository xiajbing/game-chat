package Server;

import Protobuf.DbProto;
import Server.Chat.PrivateChat;
import Server.Common.World;
import Server.Login.Login;
import Server.Login.Register;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class DbClientHandler extends SimpleChannelInboundHandler<DbProto.Msg> {

    private static ChannelHandlerContext dbServerCtx;

    public static ChannelHandlerContext getDbServerCtx(){
        return dbServerCtx;
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx){
        dbServerCtx = ctx;
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, DbProto.Msg msg){
        DbProto.Msg.MsgType type = msg.getMsgType();
        switch (type){
            case resSuc:
                Register register = new Register();
                register.registration(msg);
                break;
            case logSuc:
                Login login = new Login();
                login.loginStatus(msg);
                break;
            case priChat:
                PrivateChat privateChatDb = new PrivateChat();
                privateChatDb.savePriChat(msg);
                break;
            case init:
                World.getInstance().allId.add(msg.getPlayer().getId());
                World.getInstance().usernames.add(msg.getPlayer().getUsername());break;

            default:System.out.println(type);break;
        }
    }

}
