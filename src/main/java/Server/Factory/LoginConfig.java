package Server.Factory;

import Protobuf.ChatProto;
import Server.Cache.MsgSession;
import Server.Login.TransferMsg;
import Server.Login.Login;
import Server.Login.Register;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class LoginConfig {

    private static final Map<ChatProto.Msg.MsgType, TransferMsg> LOGIN_MAP = new ConcurrentHashMap<>();

    static {
        LOGIN_MAP.put(ChatProto.Msg.MsgType.res,new Register());
        LOGIN_MAP.put(ChatProto.Msg.MsgType.logon,new  Login());
    }

    public void loginFactory(MsgSession session, ChatProto.Msg.MsgType type){
        LOGIN_MAP.get(type).sendDbPacket(session);
    }
}
