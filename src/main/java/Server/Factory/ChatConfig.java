package Server.Factory;

import Protobuf.ChatProto;
import Server.Cache.MsgSession;
import Server.Chat.*;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ChatConfig {

    private static final Map<ChatProto.Msg.MsgType, Chat> CHAT_MAP = new ConcurrentHashMap<>();

    static{
        CHAT_MAP.put(ChatProto.Msg.MsgType.priChat, new PrivateChat());
        CHAT_MAP.put(ChatProto.Msg.MsgType.worChat,new WorldChat());
        CHAT_MAP.put(ChatProto.Msg.MsgType.teamChat,new TeamChat());
        CHAT_MAP.put(ChatProto.Msg.MsgType.unionChat,new GuildChat());
        CHAT_MAP.put(ChatProto.Msg.MsgType.mapChat,new MapChat());
    }

    public void chatFactory(MsgSession session, ChatProto.Msg.MsgType type){
        CHAT_MAP.get(type).sendPacket(session);
    }
}
