package Server.Factory;

import Protobuf.ChatProto;
import Server.Cache.MsgSession;

public class MessageFactory {

    private MessageFactory(){}

    private static class InnerUser{
        private static final MessageFactory SINGLETON = new MessageFactory();
    }

    public static MessageFactory getInstance(){
        return MessageFactory.InnerUser.SINGLETON;
    }



    public void dispatch(MsgSession session){
        int loginLimit = 2;
        ChatProto.Msg msg = session.getMsg();
        ChatProto.Msg.MsgType type = msg.getMsgType();
        if(type.getNumber()>=0&&type.getNumber()<loginLimit){
            LoginConfig loginConfig = new LoginConfig();
            loginConfig.loginFactory(session,type);
        }else {
            ChatConfig chatConfig = new ChatConfig();
            chatConfig.chatFactory(session,type);
        }
    }


}
