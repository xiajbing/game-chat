package Client;

import Protobuf.ChatProto;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.protobuf.ProtobufDecoder;
import io.netty.handler.codec.protobuf.ProtobufEncoder;
import io.netty.handler.codec.protobuf.ProtobufVarint32FrameDecoder;
import io.netty.handler.codec.protobuf.ProtobufVarint32LengthFieldPrepender;

import java.io.IOException;
import java.net.InetSocketAddress;


public class Client {
    public static void main(String[] args) throws Exception{
//        EventLoopGroup eventLoopGroup = new NioEventLoopGroup();
//        try{
//            Bootstrap bootstrap = new Bootstrap();
//            bootstrap
//                    .group(eventLoopGroup)
//                    .channel(NioSocketChannel.class)
//                    .option(ChannelOption.SO_KEEPALIVE,true)
//                    .option(ChannelOption.TCP_NODELAY,true)
//                    .option(ChannelOption.SO_RCVBUF,32*1024)
//                    .handler(new ChannelInitializer<SocketChannel>() {
//
//                        @Override
//                        protected void initChannel(SocketChannel ch) throws Exception {
//                            ch.pipeline().addLast(new ProtobufVarint32FrameDecoder());
//                            ch.pipeline().addLast(new ProtobufDecoder(ChatProto.Msg.getDefaultInstance()));//解码
//                            ch.pipeline().addLast(new ProtobufVarint32LengthFieldPrepender());
//                            ch.pipeline().addLast(new ProtobufEncoder());
//
//                            ch.pipeline().addLast(new ClientHandler());
//                        }
//                    });
//            ChannelFuture future = bootstrap.connect(new InetSocketAddress("localhost",8888)).sync();
//            future.channel().closeFuture().sync();
//        }finally {
//            eventLoopGroup.shutdownGracefully();
//            System.in.close();
//        }
        String host = "localhost";
        connect(host,8888);
    }


    public static void connect(String host, int port) throws InterruptedException {
        EventLoopGroup eventLoopGroup = new NioEventLoopGroup();
        try{
            Bootstrap bootstrap = new Bootstrap();
            bootstrap
                    .group(eventLoopGroup)
                    .channel(NioSocketChannel.class)
                    .option(ChannelOption.SO_KEEPALIVE,true)
                    .option(ChannelOption.TCP_NODELAY,true)
                    .option(ChannelOption.SO_RCVBUF,32*1024)
                    .handler(new ChannelInitializer<SocketChannel>() {

                        @Override
                        protected void initChannel(SocketChannel ch) throws Exception {
                            ch.pipeline().addLast(new ProtobufVarint32FrameDecoder());
                            ch.pipeline().addLast(new ProtobufDecoder(ChatProto.Msg.getDefaultInstance()));//解码
                            ch.pipeline().addLast(new ProtobufVarint32LengthFieldPrepender());
                            ch.pipeline().addLast(new ProtobufEncoder());

                            ch.pipeline().addLast(new ClientHandler());
                        }
                    });
            ChannelFuture future = bootstrap.connect(new InetSocketAddress(host,port)).sync();
            future.channel().closeFuture().sync();
        }finally {
            eventLoopGroup.shutdownGracefully();
            try {
                System.in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
