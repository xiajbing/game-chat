package Common;


import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class Server {

    private static final File FILE = new File("src/main/resources/server.xml");
    private static final Map<String ,Map<String,Object>> CONSTANT = XmlRead.getXml(FILE);

    public enum HostAndPort{
        //聊天服务器host，port
        CHATHOST("chatHost",CONSTANT.get("chat").get("host")),
        CHATPORT("chatPort",CONSTANT.get("chat").get("port")),

        DBHOST("dbHost",CONSTANT.get("db").get("host")),
        DBPORT("dbPort",CONSTANT.get("db").get("port"));

        HostAndPort(String name,Object val) {
            this.name = name;
            this.val = val;
        }
        private final String name;
        private final Object val;

        public String getName(){
            return name;
        }

        public Object getVal(){
            return val;
        }

        private static final Map<String, Object> MAP = new HashMap<>();

        static {
            for (HostAndPort hostAndPort : HostAndPort.values()) {
                MAP.put(hostAndPort.getName(), hostAndPort.getVal());
            }
        }

        // 根据name查询value值
        public static Object getValueByName(String name) {
            return MAP.get(name);
        }

    }

//    private final String CHATHOST;
//    private final String DBHOST;
//    private final int CHATPORT;
//    private final int DBPORT;
//
//    public Server(){
//        CHATHOST = (String) CONSTANT.get("chat").get("host");
//        CHATPORT = Integer.parseInt((String) CONSTANT.get("chat").get("port"));
//        DBHOST = (String) CONSTANT.get("db").get("host");
//        DBPORT = Integer.parseInt((String) CONSTANT.get("db").get("port"));
//    }
//
//    public String getChatHost(){
//        return CHATHOST;
//    }
//
//    public String getDbHost(){
//        return DBHOST;
//    }
//
//    public int getChatPort(){
//        return CHATPORT;
//    }
//
//    public int getDbPort(){
//        return DBPORT;
//    }


}
