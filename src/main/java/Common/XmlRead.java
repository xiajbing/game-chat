package Common;


import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class XmlRead {

    public static Map<String,Map<String,Object>> getXml(File file){
        Map<String,Map<String,Object>> constantMap = new HashMap<>(8);
        try {
            File f = new File("src/main/resources/server.xml");
            SAXReader reader = new SAXReader();
            Document doc = reader.read(f);
            Element root = doc.getRootElement();
            Element sheetNode = root.element("Worksheet");
            Element tableNode = sheetNode.element("Table");
            List table = tableNode.elements();
            List<String> key = new ArrayList<>();
            for (int i = 0;i<table.size();i++) {
                List line = ((Element) table.get(i)).elements();
                Map<String,Object> map = new HashMap<>(8);
                String serverKey = null;
                for (int j = 0;j< line.size();j++) {
                    if(i == 0){
                        key.add(((Element)line.get(j)).elementText("Data"));
                        continue;
                    }
                    if(j == 0){
                        serverKey = ((Element)line.get(j)).elementText("Data");
                        continue;
                    }
                    map.put(key.get(j),((Element)line.get(j)).elementText("Data"));
                }
                if(i!=0) {
                    constantMap.put(serverKey, map);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return constantMap;
    }


}
